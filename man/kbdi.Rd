% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/KBDI.R
\name{KBDI}
\alias{KBDI}
\title{KBDI}
\usage{
KBDI(doy, Temp, Precip, dfr)
}
\arguments{
\item{doy}{day of year}

\item{Temp}{Temperature in degrees C}

\item{Precip}{Precipitation in mm}

\item{dfr}{Dataframe containing all of the above}
}
\description{
Implementation of the Keetch Byriam Drought Index
}
\details{
The Keetch-Byram index (KBDI) number indicates the amount of net rainfall (in mm) that is required to reduce the index to zero. The initialization of KBDI usually involves setting it to zero after a period of substantial precipitation. Thus, it is necessary to go back in time until a day is reached on which it is reasonably certain that the upper soil layers were saturated to initialize the index. In snowfree areas, the authors suggest 6 to 8 inches (152.4 to 203.2 mm) in a period of a week. In areas of heavy snowfall, the best choice is probably starting the index just after the snow melts in spring. For this reason, the function has two alternative arguments: wrs, for the precipitation-based approach, or start.date, in order to set a fixed date to start the index.KBDI is an input for the subsequent computation of the McArthur’s drought code and FFDI.
}
\note{
The physical theory for the KBDI (Keetch and Byram 1968) is based on a number of assumptions. The first assumption is that soil moisture is at field capacity with a water depth equivalent to about 200mm. The second assumption is that the rate of moisture loss in an area depends on the vege- tation cover in the area, and vegetation density is a function of the mean annual rainfall. Hence, daily transpiration is approximated by an inverse exponential function of the mean annual rainfall. Finally, the evaporation rate of soil moisture with time is assumed to be an estimation of relative evapotranspiration from exponential function of the daily maximum air temperature. Sensitivity analyses from earlier researchers have revealed that KBDI decays exponentially with an assumed maximum soil water deficit and is sensitive to the daily maximum air temperature (Dennison et al. 2003). Its values range from 0 to 800 (in hundredths of inch, equivalent to 203.2mm), with 800 indicating extreme drought and zero indicating saturated soil. On the other hand, McArthur’s drought factor equation incorporates the KBDI, and it is in turn used to compute the McArthur’s forest fire danger index (FFDI), used in Australia for several decades, generally providing a good indication of the difficulty of fire suppression over a wide range of conditions.
}
\examples{
data <- read.csv("data/CUP_sem1.csv")
KBDI('DoY','T','Precip',data)
}
\references{
Dennison, P.E., Roberts, D.A., Thorgusen, S.R., Regelbrugge, J.C., Weise, D. and Christopher, L. (2003) Modeling seasonal changes in live fuel moisture and equivalent water thickness using a cumulative water balance index. Remote Sensing of Environment, 88, 442-452.
Dowdy, A.J., Mills, G.A., Finkele, K. and de Groot, W. (2010). Index sensitivity analysis applied to the Canadian Forest Fire Weather Index and the McArthur Forest Fire Danger Index. Meteoro- logical Applications, 17, 298-312.
Keetch, J.J. and Byram, G.M. (1968). A drought index for forest fire control. Tech. Rep., USDA Forest Service Research Paper SE-38, North Carolina, USA.
}
\author{
Víctor Resco de Dios \email{v.rescodedios@gmail.com}

V. Resco de Dios
}
