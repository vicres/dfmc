#' Nelson
#'
#' Calculates equilibrium dead fine fuel moisture according to Nelson 1984
#'
#'
#'@param DFFM dead fine fuel moisture content
#'@param Temp temperature
#'@param RH relative humidity
#'@param dfr a dataset containing all of the above
#'@export
#' @author 
#' Víctor Resco de Dios \email{v.rescodedios@gmail.com}
#'@examples
#' data <- read.csv("data/CUP_sem1.csv")
#' with(data,Nelson(CS505,RH,T))
#' @references
#' Nelson, R. 1984. A method for describing equilibrium moisture content. Canadian Journal of Forest Research 14: 597-600.

Nelson <- function(DFFM,RH,Temp) {
  aN<-log(RH/100)
  bN<-Temp+273.15
  cN<-log(-0.11*aN*bN)
  modNel<-lm(DFFM~cN)
  return(as.vector(predict(modNel)))
}
