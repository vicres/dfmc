#' FMI
#'
#' Calculates equilibrium dead fine fuel moisture according to Sharples 2009
#'
#'
#'@param Temp temperature
#'@param RH relative humidity
#'@export
#'@examples
#' data <- read.csv("data/CUP_sem1.csv")
#' with(data,FMI(RH,T))
#' @author 
#' Víctor Resco de Dios \email{v.rescodedios@gmail.com}
#' @references
#' Sharples, J.J., McRae, R.H.D., Weber, R.O., Gill, A.M., 2009. A simple index for assessing fuel moisture content. Environ. Model. Software 24 (5), 637–646.

FMI<-function(RH,Temp){
  FM <- 10-0.25*(Temp-RH)
  return(FM)
}